# Overview #

### Libraries Used ###

* [Couchbase Lite Android 1.1.0](https://github.com/couchbase/couchbase-lite-android/)

* appcompat-v7:22.2.0

* [android-async-http:1.4.7](https://loopj.com/android-async-http)

### Installation ###

* Open git bash on root android project folder and execute: 

      *** git submodule add git@bitbucket.org:Codeitat-Software/android-app_cblite.git ***

* Edit settings.gradle and add "android-app_cblite"

      looks like: ***include ':app', 'android-app_cblite'***

* Edit main module build.gradle and add "compile project(':android-app_cblite')" to dependencies

* Edit .gitmodules and add 'ignore = dirty' on submodule "android-app_cblite"

* Make sure you have Android SDK Build-Tools 22.0.1, Android 5.1.1 (API 22) installed using Android SDK Manager
