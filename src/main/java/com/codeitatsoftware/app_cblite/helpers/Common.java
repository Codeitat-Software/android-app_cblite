package com.codeitatsoftware.app_cblite.helpers;

import java.util.Random;

/**
 * Created by JbalTero on 8/18/2015.
 */
public class Common {
    // NOTE: Usually this should be a field rather than a method
    // variable so that it is not re-seeded every call.
    private static Random rand = new Random();

    /**
     * Returns a pseudo-random number between min and max, inclusive.
     * The difference between min and max can be at most
     * <code>Integer.MAX_VALUE - 1</code>.
     * http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java
     *
     * @param min Minimum value
     * @param max Maximum value.  Must be greater than min.
     * @return Integer between min and max, inclusive.
     * @see java.util.Random#nextInt(int)
     */
    public static int randInt(int min, int max) {
        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static double randDouble(double min, double max){
        return min + (max - min) * rand.nextDouble();
    }

    public static String bytesToUnitFormat(long bytes) {
        int u = 0;
        for (;bytes > 1024*1024; bytes >>= 10) {
            u++;
        }
        if (bytes > 1024)
            u++;
        return String.format("%.1f %cB", bytes/1024f, " kMGTPE".charAt(u));
    }
}
