package com.codeitatsoftware.app_cblite.helpers;

import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.Application;
import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.UnsavedRevision;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by JbalTero on 5/9/2015.
 */
public class DbActiveRecord {

    private static CouchbaseLite couchbaseLite;
    private static Database database;

    /**
     * Empty constructor, no initialization
     */
    public DbActiveRecord() {}

    /**
     * Init on Application.initDatabase() to avoid errors.
     */
    public static void init() {
        couchbaseLite = Application.getInstance().getCouchbaseLite();
        database = couchbaseLite.getDatabase();
    }

    /**
     * Create a document with default id.
     *
     * @param data to be inserted into the document.
     * @throws CouchbaseLiteException
     */
    public static void create(HashMap<String, Object> data) throws CouchbaseLiteException{

        Document document = database.createDocument();
        document.putProperties(data);

        AppLog.d("Document created with Id: " + document.getId());
    }

    /**
     * Create a document with specified id.
     *
     * @param data to be inserted into the document
     * @param id for the document.
     * @throws CouchbaseLiteException
     */
    public static void create(HashMap<String, Object> data, String id) throws CouchbaseLiteException{
        Document document = database.getDocument(id);
        document.putProperties(data);
        AppLog.d("Document created with id: " + document.getId());
    }

    /**
     *  Update a document specified with id.
     *
     * @param docId
     * @param data to be updated into the document.
     * @return true if successfully updated, else false.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static boolean update(final String docId, final HashMap<String, Object> data) throws NullPointerException, CouchbaseLiteException{

        AppLog.d("Updating document: " + docId);

        final Document doc = database.getDocument(docId);
        doc.update(new Document.DocumentUpdater() {
            @Override
            public boolean update(UnsavedRevision newRevision) {

                Map<String, Object> properties = newRevision.getUserProperties();

                // this will update appending changes regardless of current properties
                properties.putAll(doc.getProperties());

                //loop data here and grab keys and values
                Iterator i = data.entrySet().iterator();
                while (i.hasNext()) {
                    Map.Entry entry = (Map.Entry) i.next();
                    properties.put(entry.getKey().toString(), entry.getValue());
                }

                newRevision.setUserProperties(properties);

                return true;
            }
        });

        return false;
    }

    /**
     *  Delete a document specified with id
     *
     * @param docId
     * @return true if successfully deleted, else false.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static boolean delete(String docId) throws NullPointerException, CouchbaseLiteException{
        AppLog.d("Deleting document: " + docId);

        Document document = database.getDocument(docId);

        if(document.delete()){
            AppLog.d("Document deleted.");
            return true;
        }

        return false;
    }

    /**
     * Get the string value of a property in a document
     *
     * @param docId
     * @param property
     * @return String value of property in a document
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static String getString(String docId, String property) throws NullPointerException, CouchbaseLiteException{
        String value = null;

        Document doc = database.getDocument(docId);
        value = doc.getProperty(property).toString();

        return value;
    }

    /**
     * Get the long value of a property in a document
     *
     * @param docId
     * @param property
     * @return long value of a property in a document.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static long getLong(String docId, String property) throws NullPointerException, CouchbaseLiteException{
        long value = -1;

        Document doc = database.getDocument(docId);
        value = Long.parseLong(doc.getProperty(property).toString());

        return value;
    }

    /**
     * Get the int value of a property in a document
     *
     * @param docId
     * @param property
     * @return int value of a property in a document.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static int getInteger(String docId, String property) throws NullPointerException, CouchbaseLiteException{
        int value = -1;

        Document doc = database.getDocument(docId);
        value = Integer.parseInt(doc.getProperty(property).toString());

        return value;
    }

    /**
     * Get the float value of a property in a document
     *
     * @param docId
     * @param property
     * @return float value of a property in a document.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static float getFloat(String docId, String property) throws NullPointerException, CouchbaseLiteException{
        float value = -1f;

        Document doc = database.getDocument(docId);
        value = Float.parseFloat(doc.getProperty(property).toString());

        return value;
    }

    /**
     * Get the boolean value of a property in a document
     *
     * @param docId
     * @param property
     * @return boolean value of a property in a document.
     * @throws NullPointerException
     * @throws CouchbaseLiteException
     */
    public static boolean getBoolean(String docId, String property) throws NullPointerException, CouchbaseLiteException{

        Document doc = database.getDocument(docId);
        boolean value = Boolean.parseBoolean(doc.getProperty(property).toString());

        return value;
    }

    /**
     * Check if a document specified with id exists.
     *
     * @param id for the existing document
     * @return true or false
     */
    public static boolean docExists(String id){

        try {
            Document doc = database.getExistingDocument(id);

            if (!String.valueOf(doc).equals("null")) {
                AppLog.d("Document exists. id: " + id);
                return true;
            }
            else{
                AppLog.d("Document does not exist. id: " + id);
            }
        } catch (NullPointerException e) {
            AppLog.e(e);
        }

        return false;
    }

    /**
     * The user-defined properties, without the ones reserved by CouchDB.
     * This is based on -properties, with every key whose name starts with "_" removed.
     *
     * @param id
     * @return properties
     */
    public static Map<String, Object> getUserProperties(String id) throws NullPointerException, CouchbaseLiteException{
        return database.getDocument(id).getUserProperties();
    }

    public static Document getDocument(String docId) {
        Document doc = database.getDocument(docId);
        return doc;
    }
}
