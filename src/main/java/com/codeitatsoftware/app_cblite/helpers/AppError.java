package com.codeitatsoftware.app_cblite.helpers;

/**
 * Created by Jonacius on 5/31/2015.
 */

/**
 * A lazily efficient way to put in validations and what not.
 *
 */
public class AppError {

    public static String CHARACTERS = "characters";

    public static String DIGITS = "digits";

    public static String LETTERS = "letters";

    /**
     * @param name
     * @return invalid input
     */
    public static String invalid(String name){
        return "Invalid " + name;
    }

    /**
     * @param what
     * @param lessThan
     * @param type
     * @return input is too short
     */
    public static String tooShort(String what, int lessThan, String type){
        what = what.substring(0, 1).toUpperCase() + what.substring(1);
        return what + " input is too short. Input must not be less than " + lessThan + " " + type + ".";
    }

    /**
     * @param what
     * @param moreThan
     * @param type
     * @return input is too long
     */
    public static String tooLong(String what, int moreThan, String type){
        what = what.substring(0, 1).toUpperCase() + what.substring(1);
        return what + " input is too long. Input must not be more than " + moreThan + " " + type + ".";
    }

    /**
     * @param what
     * @return first and second inputs don't match.
     */
    public static String mismatch(String what){
        what = what.substring(0, 1).toUpperCase() + what.substring(1);
        return what + " inputs do not match.";
    }

    public static String mustNotBeEmpty(String what){
        what = what.substring(0, 1).toUpperCase() + what.substring(1);
        return what + " must not be empty.";
    }
}
