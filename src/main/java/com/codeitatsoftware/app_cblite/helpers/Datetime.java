package com.codeitatsoftware.app_cblite.helpers;

import com.codeitatsoftware.app_cblite.core.AppLog;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by JbalTero on 5/21/2015.
 */
public class Datetime {

    public static String FORMAT_HH_MM_A = "hh:mm a";
    public static String FORMAT_ISO_YYYY_MM_DD = "yyyy-MM-dd";
    public static String FORMAT_YYYY_MM_DD_HH_MM_A = "yyyy-MM-dd hh:mm a";
    public static String FORMAT_UTC_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSSSSZ";
    public static String FORMAT_MEDIUM_WEEK = "MMM d (ccc), yyyy hh:mm a";
    public static String FORMAT_SHORT_WEEK = "MMM d (ccc), yy";
    // @todo Add other date format here...

    /**
     * @todo document: @return current timestamp
     */
    public static long currentTimestamp(){
        long epoch = System.currentTimeMillis()/1000;
        return epoch;
    }

    /**
     * @todo document: @return current year with week of year
     */
    public static String currentYearWithWeekYear(){

        DateTime dateTime = new DateTime(System.currentTimeMillis());
        int year = dateTime.getYear();
        int weekYear = dateTime.getWeekOfWeekyear();
        String weekDate = year + "-" + weekYear;

        return weekDate;
    }

    /**
     * @todo document: @return current timestamp of the day
     */
    public static String currentDay(){
        Calendar c = Calendar.getInstance();
        int dayOfWeek = c.get(Calendar.DAY_OF_WEEK);

        switch (dayOfWeek){
            case Calendar.MONDAY:
                return "Monday";
            case Calendar.TUESDAY:
                return "Tuesday";
            case Calendar.WEDNESDAY:
                return "Wednesday";
            case Calendar.THURSDAY:
                return "Thursday";
            case Calendar.FRIDAY:
                return "Friday";
            case Calendar.SATURDAY:
                return "Saturday";
            case Calendar.SUNDAY:
                return "Sunday";
        }

        return null;
    }

    /**
     * @todo can be retrieved using Calendar class @return current date of the day
     */
    public static String currentDateOfDay(){
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_YEAR);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        return year + "-" + month + "-" + day;
    }

    /**
     * @return date of next day
     * NOTE: day of year, not day of month
     */
    public static String dateOfNextDay(){
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DAY_OF_YEAR);
        day += 1;
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        return year + "-" + month + "-" + day;
    }

    /**
     * @return current date of the week
     */
    public static String currentDateOfWeek(){
        Calendar c = Calendar.getInstance();
        int weekOfMonth = c.get(Calendar.WEEK_OF_YEAR);
        int month = c.get(Calendar.MONTH);
        int year = c.get(Calendar.YEAR);

        return year + "-" + month + "-" + weekOfMonth;
    }

    public static long date_to_timestamp(String stringDate, String fromFormat){

        Date date = null;
        try {
            date = new SimpleDateFormat(fromFormat, Locale.US).parse(stringDate);
        } catch (ParseException e) {
            AppLog.e(e);
        }

        // @todo May produce NullPointerException
        return new Timestamp(date.getTime()).getTime();
    }

    /**
     * @todo is this epoch? (Millis or seconds?)
     * @param timestamp to be converted
     * @return Date based on timestamp
     */
    public static Date timestamp_to_date(long timestamp) {
        return new Date(timestamp * 1000L);
    }

    /**
     * @todo
     * @param dateString to be parsed
     * @param format of dateString
     * @return Date object
     * @throws ParseException if format does not match dateString or vice versa
     */
    public static Date string_to_date_format(String dateString, String format) throws ParseException {
        DateTimeFormatter parser = DateTimeFormat.forPattern(format);
        DateTime dateTime = parser.parseDateTime(dateString);
        return dateTime.toDate();
    }

    /**
     * @todo What is this for?
     * @param duration
     * @return converted value of hours in timestamp format
     */
    public static long hourDurationToTimestamp(int duration){
        long epoch = TimeUnit.HOURS.toSeconds(duration);
        return epoch;
    }

    /**
     * @todo What is this for?
     *
     * @param duration
     * @return converted value of minutes in timestamp format
     */
    public static long minuteDurationToTimestamp(int duration){
        long epoch = TimeUnit.MINUTES.toSeconds(duration);
        return epoch;
    }

    /**
     * @todo What is this for?
     *
     * @param duration
     * @return converted value of seconds in timestamp format
     */
    public static long secondDurationToTimestamp(int duration){
        long epoch = TimeUnit.SECONDS.toSeconds(duration);
        return epoch;
    }

    /**
     * Convert timestamp to String date format
     *
     * @param timestamp long to be converted
     * @param format to use for conversion
     * @return String formatted date
     */
    public static String toFormat(long timestamp, String format) {
        // init our date formatter
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());

        // get date from timestamp
        Date date = timestamp_to_date(timestamp);

        // format date using simpleDateFormat
        return simpleDateFormat.format(date);
    }

    /**
     * Convert timestamp to String date format
     *
     * @param timestamp string to be converted
     * @param format to use for conversion
     * @return String formatted date
     */
    public static String toFormat(String timestamp, String format) {
        // init our date formatter
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format, Locale.getDefault());

        // get date from timestamp
        Date date = timestamp_to_date(Long.parseLong(timestamp));

        // format date using simpleDateFormat
        return simpleDateFormat.format(date);
    }
}
