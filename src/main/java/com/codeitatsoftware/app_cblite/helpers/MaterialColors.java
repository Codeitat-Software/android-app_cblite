package com.codeitatsoftware.app_cblite.helpers;

import android.graphics.Color;

/**
 * Created by Jonacius on 8/9/2015.
 */
public class MaterialColors {

    // Red
    public static final int RED = Color.rgb(244, 68, 51);

    public static final int RED_LIGHT_1 = Color.rgb(239, 83, 80);

    public static final int RED_LIGHT_2 = Color.rgb(229, 115, 115);

    public static final int RED_LIGHT_3 = Color.rgb(229, 115, 115);

    public static final int RED_LIGHT_4 = Color.rgb(255, 205, 210);

    public static final int RED_LIGHT_5 = Color.rgb(255, 235, 238);

    public static final int RED_DARK_1 = Color.rgb(229, 57, 53);

    public static final int RED_DARK_2 = Color.rgb(198, 40, 40);

    public static final int RED_DARK_3 = Color.rgb(183, 28, 28);

}
