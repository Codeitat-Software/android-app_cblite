package com.codeitatsoftware.app_cblite.helpers;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.couchbase.lite.Document;
import com.couchbase.lite.LiveQuery;
import com.couchbase.lite.QueryEnumerator;

public abstract class LiveQueryRecyclerAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter {

    protected Context mContext;
    private LiveQuery mLiveQuery;
    private QueryEnumerator mQueryEnumerator;

    public LiveQueryRecyclerAdapter(Context context, LiveQuery query) {
        mContext = context;
        mLiveQuery = query;
        mLiveQuery.addChangeListener(new LiveQuery.ChangeListener() {
            @Override
            public void changed(final LiveQuery.ChangeEvent event) {
                ((Activity) LiveQueryRecyclerAdapter.this.mContext).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mQueryEnumerator = event.getRows();
                        notifyDataSetChanged();
                    }
                });
            }
        });
        mLiveQuery.start();

    }

    public LiveQuery getLiveQuery() {
        return this.mLiveQuery;
    }

    @Override
    public int getItemCount() {
        return this.mQueryEnumerator != null ? this.mQueryEnumerator.getCount() : 0;
    }

    public Object getItem(int i) {
        if (this.mQueryEnumerator != null){
            // get document
            Document document = this.mQueryEnumerator.getRow(i).getDocument();
            // if document is not null
            if(document != null){
                return document;
            }
            // else try to look in the value
            else{
                try {
                    // get value id
                    String id = (String) this.mQueryEnumerator.getRow(i).getValue();
                    // if id is not null
                    if(id != null){
                        return DbActiveRecord.getDocument(id);
                    }
                    else return null;
                } catch (Exception e) {
                    return null;
                }
            }
        }
        else return null;
    }
}
