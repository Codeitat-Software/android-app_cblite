package com.codeitatsoftware.app_cblite.helpers;

import android.content.Context;
import android.content.SharedPreferences;

import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;

/**
 * Created by JbalTero on 9/7/2015.
 */
public class PrefsManager{

    public static final String PREF_NAME = CouchbaseLite.DATABASE_NAME + "_pref";

    private static PrefsManager sInstance;
    private final SharedPreferences mPref;

    private PrefsManager(Context context) {
        mPref = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);

        AppLog.v("Instantiated. " + PREF_NAME + " initialized.");
    }

    /**
     * Initialize singleton instance
     *
     * @param context
     */
    public static synchronized void init(Context context) {
        if (sInstance == null) {
            sInstance = new PrefsManager(context);
        }
    }

    /**
     * @return singleton instance's SharedPreferences
     */
    public static synchronized SharedPreferences getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(PrefsManager.class.getSimpleName() +
                    " is not initialized, call init(..) method first.");
        }
        return sInstance.mPref;
    }
}
