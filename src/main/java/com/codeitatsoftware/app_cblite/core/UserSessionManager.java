package com.codeitatsoftware.app_cblite.core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.codeitatsoftware.app_cblite.helpers.PrefsManager;
import com.couchbase.lite.CouchbaseLiteException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

// http://androidexample.com/Android_Session_Management_Using_SharedPreferences_-_Android_Example/index.php?view=article_discription&aid=127&aaid=147
public class UserSessionManager{

    // Singleton instance
    private static UserSessionManager sInstance;

	// Shared Preferences reference
	SharedPreferences pref;

	// Editor reference for Shared preferences
	Editor editor;

	// Context
	Context _context;

	// All Shared Preferences Keys
	private static final String IS_USER_LOGIN = "IsUserLoggedIn";

    // User data
	public static final String KEY_USERNAME = "username";
	public static final String KEY_PASSWORD = "password";
    public static final String KEY_EMAIL = "email";
	public static final String KEY_ROLES = "roles";
    public static final String KEY_LAST_SEQUENCE = "last_sequence_number";

    // Session Cookie
    public static final String KEY_SESSION_ID = "session_id";
    public static final String KEY_EXPIRES = "expires";
    public static final String KEY_COOKIE_NAME = "cookie_name";

    /**
     * Initialize singleton instance
     *
     * @param context
     */
    public static synchronized void init(Context context) {
        if (sInstance == null) {
            sInstance = new UserSessionManager(context);
        }
    }

    /**
     * @return singleton instance
     */
    public static UserSessionManager getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(UserSessionManager.class.getSimpleName() +
                    " is not initialized, call init(..) method first.");
        }
        return sInstance;
    }

    /**
     * Constructor and Init Shared Preferences
     *
     * @param context
     */
	public UserSessionManager(Context context) {
		this._context = context;
		pref = PrefsManager.getInstance();
		editor = PrefsManager.getInstance().edit();

        AppLog.v("Instantiated");
	}

    /**
     * Create session cookie
     *
     * @param response
     * @param username
     * @throws JSONException
     */
    public void createNewSession(JSONObject response, String username) throws JSONException {
        // get session object
        JSONObject session = response.getJSONObject("session");
        // get roles array
        JSONArray roles = response.getJSONArray("roles");
        Set<String> rolesSet = new HashSet<>();

        // convert roles JSONArray to Set<String>
        for(int i = 0; i < roles.length(); i++){
            rolesSet.add(roles.get(i).toString());
        }

        putSessionCookie(session);

        // Storing email in pref
        editor.putString(KEY_EMAIL, response.getString(KEY_EMAIL));

        // Storing name in pref
        editor.putString(KEY_USERNAME, username);

        // Storing login value as TRUE
        editor.putBoolean(IS_USER_LOGIN, true);

        // Storing role in pref
        editor.putStringSet(KEY_ROLES, rolesSet);

        // apply changes
        editor.apply();

        AppLog.i("Successfully authenticated with session_id: " + session.getString(KEY_SESSION_ID));
    }

	/**
	 * Check login method will check user login status
	 * If false it will redirect user to login page
	 * Else do anything
	 */
	public boolean checkLogin() {
        return !this.isUserLoggedIn();
    }

	/**
	 * Get stored user session data
	 */
	public HashMap<String, String> getUserSessionData() {
		//Use hashmap to store user credentials
		HashMap<String, String> user = new HashMap<String, String>();

		// user name
		user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));

		// user password
		user.put(KEY_PASSWORD, pref.getString(KEY_PASSWORD, null));

        // user email
        user.put(KEY_PASSWORD, pref.getString(KEY_EMAIL, null));

		// user role
		user.put(KEY_ROLES, pref.getString(KEY_ROLES, null));

        // user last sequence number
        user.put(KEY_LAST_SEQUENCE, pref.getString(KEY_LAST_SEQUENCE, null));

        // user session cookie name
        user.put(KEY_COOKIE_NAME, pref.getString(KEY_COOKIE_NAME, null));

        // user session id
        user.put(KEY_SESSION_ID, pref.getString(KEY_SESSION_ID, null));

        // user session expiration date
        user.put(KEY_EXPIRES, pref.getString(KEY_EXPIRES, null));

		AppLog.v("User details: " + user.toString());

		// return user
		return user;
	}

    /**
     * Called on logout after successfully deleted the session on server.
     * Deletes the database,
     * Clears all user data from Shared Pref
     * Delete log file.
     *
     * @param context
     * @param intent
     */
    public void clear(Context context, Intent intent) throws CouchbaseLiteException {
        CouchbaseLite
                .getInstance()
                .getSynchronize()
                .destroyReplications();

        CouchbaseLite
                .getInstance()
                .deleteDatabase();

        // Clearing SharedPreferences
        editor.clear();
        editor.apply();
        AppLog.d(PrefsManager.PREF_NAME + " deleted.");

        // Make sure the log file was deleted.
        if(AppLog.deleteLog(context)){
            AppLog.i("User Logged out");

            Activity activity = (Activity) context;
            context.startActivity(intent);

            activity.finish();
        }
    }

    // get roles
    public Set<String> getRoles(){
        return pref.getStringSet(KEY_ROLES, new HashSet<String>());
    }

    // get cookie_name
    public String getCookieName(){
        return pref.getString(KEY_COOKIE_NAME, null);
    }

    // get session_id
    public String getSessionId(){
        return pref.getString(KEY_SESSION_ID, null);
    }

    // put session cookie
    public void putSessionCookie(JSONObject session) throws JSONException {
        // Storing name in pref
        editor.putString(KEY_COOKIE_NAME, session.getString(KEY_COOKIE_NAME));

        // Storing password in pref
        editor.putString(KEY_SESSION_ID, session.getString(KEY_SESSION_ID));

        // Storing role in pref
        editor.putString(KEY_EXPIRES, session.getString(KEY_EXPIRES));

        // apply changes
        editor.apply();
    }

    // get expires
    public String getExpires(){
        return pref.getString(KEY_EXPIRES, null);
    }

	// Check for login
	public boolean isUserLoggedIn() {
		return pref.getBoolean(IS_USER_LOGIN, false);
	}

	// get User Name
	public String getUsername() {
		return pref.getString(KEY_USERNAME, null);
	}

    public void putLastSequenceNumber(long number){
        // Storing pincode in pref
        editor.putLong(KEY_LAST_SEQUENCE, number);

        // apply changes
        editor.apply();
    }

    public long getLastSequenceNumber(){
        return pref.getLong(KEY_LAST_SEQUENCE, -1);
    }

    public void putAfterLogin(boolean afterLogin) {
        editor.putBoolean("afterLogin", afterLogin);

        // apply changes
        editor.apply();
    }

    public boolean getAfterLogin() {
        return pref.getBoolean("afterLogin", false);
    }

    public void putLastSyncDateTime(String timestamp) {
        editor.putString("LastSyncDateTime", timestamp);

        // apply changes
        editor.apply();
    }

    public String getLastSyncDateTime() {
        return pref.getString("LastSyncDateTime", null);
    }

    public String getEmail() {
        return pref.getString(KEY_EMAIL, null);
    }

}
