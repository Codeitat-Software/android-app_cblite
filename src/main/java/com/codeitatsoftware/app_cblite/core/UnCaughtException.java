package com.codeitatsoftware.app_cblite.core;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.StatFs;
import android.util.Log;

import com.codeitatsoftware.app_cblite.helpers.Common;
import com.codeitatsoftware.app_cblite.helpers.Datetime;
import com.codeitatsoftware.app_cblite.helpers.DbActiveRecord;
import com.codeitatsoftware.app_cblite.ui.CrashReport;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by jbaltero on 1/30/15.
 */
public class UnCaughtException implements Thread.UncaughtExceptionHandler {

    private Application mApp = Application.getInstance();
    private Context mContext;

    public UnCaughtException(Context context) {
		this.mContext = context;

        AppLog.v("Instantiated");
	}

	public void uncaughtException(Thread t, Throwable e) {
        Log.e(AppLog.TAG, Log.getStackTraceString(e));

        UserSessionManager userSessionManager = mApp.getUserSessionManager();

        try {
            StringBuilder report = new StringBuilder();

            if (userSessionManager.isUserLoggedIn()) {
                report.append("User: ").append(userSessionManager.getUsername()).append('\n');
                report.append("Email: ").append(userSessionManager.getEmail()).append('\n');
                report.append("Roles: ").append(userSessionManager.getRoles()).append('\n');
                report.append("Session ID: ").append(userSessionManager.getSessionId()).append('\n').append('\n');
            }

            report.append("Stack:\n");
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			e.printStackTrace(printWriter);
			report.append(result.toString());
			printWriter.close();
			report.append('\n').append('\n');

            addInformation(report);

            long timestamp = Datetime.currentTimestamp();

            // Create document for crash report
            HashMap<String, Object> data = new HashMap<>();
            data.put("type", "report");
            data.put("level", "crash");
            data.put("message", report.toString());
            data.put("created_on", Datetime.toFormat(timestamp, Datetime.FORMAT_YYYY_MM_DD_HH_MM_A));
            data.put("created_by", mApp.getUserSessionManager().getUsername());
            DbActiveRecord.create(data, String.valueOf(timestamp) + "-report");
            AppLog.d("Crash report document created.");

			Intent crashedIntent = new Intent(mContext, CrashReport.class);
			crashedIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			crashedIntent.putExtra("message", report.toString());
			mContext.startActivity(crashedIntent);

		} catch (Throwable ignore) {
			AppLog.e(ignore);
		}

		System.exit(0); // kill off the crashed app
	}

	// ====== Helpers =====

	private StatFs getStatFs() {
		File path = Environment.getDataDirectory();
		return new StatFs(path.getPath());
	}

	private long getAvailableInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long availableBlocks = stat.getAvailableBlocks();
		return availableBlocks * blockSize;
	}

	private long getTotalInternalMemorySize(StatFs stat) {
		long blockSize = stat.getBlockSize();
		long totalBlocks = stat.getBlockCount();
		return totalBlocks * blockSize;
	}

	private void addInformation(StringBuilder message) {
		message.append("Locale: ").append(Locale.getDefault()).append('\n');
		try {
			PackageManager pm = mContext.getPackageManager();
			PackageInfo pi;
			pi = pm.getPackageInfo(mContext.getPackageName(), 0);
			message.append("VersionName: ").append(pi.versionName).append('\n');
            message.append("VersionCode: ").append(pi.versionCode).append('\n');
			message.append("Package: ").append(pi.packageName).append('\n');
		} catch (Exception e) {
			AppLog.e(e);
			message.append("Could not get Version information for ").append(
                    mContext.getPackageName());
		}
		message.append("Phone Model: ").append(android.os.Build.MODEL)
				.append('\n');
		message.append("Android Version: ")
				.append(android.os.Build.VERSION.RELEASE).append('\n');
		StatFs stat = getStatFs();
		message.append("Total Internal memory: ")
				.append(Common.bytesToUnitFormat(getTotalInternalMemorySize(stat))).append('\n');
		message.append("Available Internal memory: ")
				.append(Common.bytesToUnitFormat(getAvailableInternalMemorySize(stat))).append('\n');
	}

}
