package com.codeitatsoftware.app_cblite.core;

import android.content.SharedPreferences;

import com.codeitatsoftware.app_cblite.BuildConfig;
import com.codeitatsoftware.app_cblite.core.cblite.AppServer;
import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.codeitatsoftware.app_cblite.helpers.AutoUpdateApk;
import com.codeitatsoftware.app_cblite.helpers.PrefsManager;

import net.danlew.android.joda.JodaTimeAndroid;

/**
 * Created by jbaltero on 11/28/14.
 * https://www.infinum.co/the-capsized-eight/articles/server-client-syncing-for-mobile-apps-using-couchbase-mobile
 */
public abstract class Application extends android.app.Application {

    /**
     * What's the name of your app?
     * This will be used by logging tags and etc.
     */
    public static String APP_NAME;

    /**
     * What environment are you in?
     * This is just a state where you can develop your own logic depending on the environment.
     * For example:
     *      if(Application.ENVIRONMENT == env.DEVELOPMENT){
     *          // Use development server
     *      }
     *      else{
     *          // Use production server
     *      }
     */
	public static env ENVIRONMENT;

    /**
     * Environment options
     */
    public enum env{
        PRODUCTION,
        DEVELOPMENT
    }

	/**
	 * This will be called on UnCaughtException class as sender error email.
	 */
	public static String DEVELOPER_EMAIL;

	/**
	 * A singleton instance of the application class for easy access in other places
	 */
	private static Application sInstance;

    /**
     * @return ApplicationController singleton instance
     */
    public static synchronized Application getInstance() {
        return sInstance;
    }

    /**
	 * Called when the application is starting, before any activity, service,
	 * or receiver objects (excluding content providers) have been created.
	 * Implementations should be as quick as possible (for example using
	 * lazy initialization of state) since the time spent in this function
	 * directly impacts the performance of starting the first activity,
	 * service, or receiver in a process.
	 * If you override this method, be sure to call super.onCreate().
	 */
	@Override
	public void onCreate() {
		super.onCreate();

		// initialize the singleton
		sInstance = this;

        setEnvironment();

        // set app name
        APP_NAME = app_name();

        // set developer email
        DEVELOPER_EMAIL = developer_email();

		AppLog.i(APP_NAME + " starting. " + ENVIRONMENT.name() + " ENVIRONMENT");

		// Crash Report handler
		Thread.setDefaultUncaughtExceptionHandler(new UnCaughtException(getApplicationContext()));

        // initialize SharedPreferences Manager
        PrefsManager.init(getApplicationContext());

        initUserSessionManager();

        initCouchbaseLite();

        // Must initialize the timezone provider and TIMEZONE_CHANGED broadcast receiver with a Context (via JodaTimeAndroid.init())
        // https://github.com/dlew/joda-time-android#usage
        JodaTimeAndroid.init(this);
	}

    /**
     * @return Application name used for logcat tag, database name, and etc.
     */
    protected abstract String app_name();

    /**
     * @return Developer email for crash reporting
     */
    protected abstract String developer_email();

    private void initUserSessionManager(){
        // preference flag
        PrefsManager.getInstance().edit().putBoolean("initUserSessionManager", true).apply();

        // initialize UserSessionManager
        UserSessionManager.init(getApplicationContext());
    }

    private void initCouchbaseLite(){
        // initialize CouchbaseLite
        CouchbaseLite.init();
    }

    public void initAutoUpdateApk(){
        // preference flag
        PrefsManager.getInstance().edit().putBoolean("initAutoUpdateApk", true).apply();

        //https://code.google.com/p/auto-update-apk-client/#How_to_use_AutoUpdateApk
        AutoUpdateApk.init(getApplicationContext());

        // init from preferences
        if(PrefsManager.getInstance().getBoolean("enableMobileUpdates", false))
            AutoUpdateApk.enableMobileUpdates();
        else
            AutoUpdateApk.disableMobileUpdates();
    }

    public void initAppServer(String server_host, String web_app_server_host, String port){
        AppServer.init(server_host, web_app_server_host, port);
    }

    /**
     * @return PrefsManager instance
     */
    public SharedPreferences getPrefsManager(){
        return PrefsManager.getInstance();
    }

	/**
	 * @return CouchbaseLite instance
	 */
	public CouchbaseLite getCouchbaseLite() {
		return CouchbaseLite.getInstance();
	}

	/**
	 * @return UserSessionManager instance
	 */
	public UserSessionManager getUserSessionManager() {
		return UserSessionManager.getInstance();
	}

    /**
     * https://code.google.com/p/auto-update-apk-client
     * @return
     */
    public AutoUpdateApk getAutoUpdateApk() {
        return AutoUpdateApk.getInstance();
    }

    /**
     * Set environment based on build_type
     */
    private void setEnvironment() {
        // release = production
        if(BuildConfig.BUILD_TYPE.equals("release")){
            ENVIRONMENT = env.PRODUCTION;
        }
        // debug = development
        else if(BuildConfig.BUILD_TYPE.equals("debug")){
            ENVIRONMENT = env.DEVELOPMENT;
        }
    }
}
