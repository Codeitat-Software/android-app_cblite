package com.codeitatsoftware.app_cblite.core.base;

import android.os.Parcel;
import android.os.Parcelable;

import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.codeitatsoftware.app_cblite.core.exceptions.DocPropertyValidationException;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Document;
import com.couchbase.lite.SavedRevision;
import com.couchbase.lite.UnsavedRevision;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by JbalTero on 8/20/2015.
 */
public abstract class BaseModel implements Parcelable{
    private CouchbaseLite mCouchbaseLite = CouchbaseLite.getInstance();
    protected final Database mDatabase = mCouchbaseLite.getDatabase();

    // Doc
    private Document mDocument;
    // Doc Properties
    private Map<String, Object> mProperties = new HashMap<>();

    // KEY
    public static String KEY_ID = "_id";
    public static String KEY_REV = "_rev";
    public static String KEY_TYPE = "type";
    public static String KEY_CHANNELS = "channels";

    public enum Validation_Rules{
        REQUIRED
        // TODO: 8/21/2015 Add validation rules
    }

    /**
     * Just an empty constructor for creating doc
     */
    public BaseModel(){}

    /**
     * @param id of doc to read
     */
    public BaseModel(String id) throws DocPropertyValidationException {
        read(id);
    }

    /**
     * @param document to be parsed in this model class
     */
    public BaseModel(Document document) throws DocPropertyValidationException {
        // get doc properties
        Map<String, Object> properties = document.getProperties();

        // make sure document can be parsed in this model
        runValidation(properties);

        mProperties = properties;
        mDocument = document;
    }

    /**
     * Constructor if the child class implements parcelable
     *
     * @param in
     */
    public BaseModel(Parcel in) {
        in.readMap(mProperties, Object.class.getClassLoader());
        // get doc id from properties
        mDocument = mDatabase.getDocument((String) mProperties.get(KEY_ID));
    }

    /**
     * Create new doc with an automatically-assigned UUID
     *
     * @param data to be created
     * @throws CouchbaseLiteException
     * @throws NullPointerException if data is null
     * @throws DocPropertyValidationException during validation
     */
    public void create(Map<String, Object> data) throws CouchbaseLiteException, NullPointerException, DocPropertyValidationException {
        // Make sure it is not null
        if (data == null) {
            throw new NullPointerException("Data to be created should not be null.");
        }
        else{
            runValidation(data);

            SavedRevision savedRevision = mDatabase.createDocument().putProperties(data);

            mProperties = savedRevision.getProperties();
            mDocument = savedRevision.getDocument();

            // callback
            onDocCreated();
        }
    }

    /**
     * Create new doc with a custom ID.
     *
     * @param data to be created
     * @param id of the document to be created
     * @throws CouchbaseLiteException
     * @throws NullPointerException if data is null
     * @throws DocPropertyValidationException during validation
     */
    public void create(Map<String, Object> data, String id) throws DocPropertyValidationException, CouchbaseLiteException, NullPointerException {

        // Make sure id is not null nor empty
        if(id.equals(null) || id.isEmpty()){
            throw new NullPointerException("ID of the doc to be created should not be null or empty.");
        }

        // Make sure it is not null
        if (data == null) {
            throw new NullPointerException("Data to be created should not be null.");
        }
        else{
            runValidation(data);

            SavedRevision savedRevision = mDatabase.getDocument(id).putProperties(data);

            mProperties = savedRevision.getProperties();
            mDocument = savedRevision.getDocument();

            // callback
            onDocCreated();
        }
    }

    /**
     * Called after {@link BaseModel#create(Map)} doc.
     * savedRevision properties and doc are already been set in {@link BaseModel#mProperties}
     *
     */
    protected abstract void onDocCreated();

    /**
     * Read existing doc specified with id.
     *
     * @param id of existing document
     */
    private void read(String id) throws DocPropertyValidationException {
        // get existing doc
        Document document = mDatabase.getExistingDocument(id);
        // get doc properties
        Map<String, Object> properties = document.getProperties();

        // make sure document can be parsed in this model
        runValidation(properties);

        // set properties
        mProperties = properties;
        // set document
        mDocument = document;
    }

    public void update(final Map<String, Object> data) throws CouchbaseLiteException {
        // get existing document
        final Document doc = mDatabase.getExistingDocument(getID());

        // doc updater
        doc.update(new Document.DocumentUpdater() {
            @Override
            public boolean update(UnsavedRevision newRevision) {
                Map<String, Object> properties = newRevision.getUserProperties();

                // this will update appending changes regardless of current properties
                properties.putAll(doc.getProperties());

                //loop data here and grab keys and values
                Iterator i = data.entrySet().iterator();
                while (i.hasNext()) {
                    Map.Entry entry = (Map.Entry) i.next();
                    properties.put(entry.getKey().toString(), entry.getValue());
                }

                newRevision.setUserProperties(properties);

                // set updated properties and document
                mProperties = newRevision.getProperties();
                mDocument = newRevision.getDocument();

                onDocUpdated();

                return true;
            }
        });
    }

    protected abstract void onDocUpdated();

    public void delete() throws CouchbaseLiteException {
        // Make sure properties and document is not null
        if(mProperties == null && mDocument == null){
            throw new NullPointerException("No document to delete.");
        }
        // if properties is not null, it means doc is not yet loaded.
        else if(mProperties != null){
            // load document
            mDocument = mDatabase.getExistingDocument((String) mProperties.get(KEY_ID));

            mDocument.delete();
            onDocDeleted();
        }
    }

    protected abstract void onDocDeleted();

    public Object getProperty(String key, Object defaultValue){
        if(mProperties == null) return defaultValue;
        else return mProperties.containsKey(key)? mProperties.get(key) : defaultValue;
    }

    public Map<String, Object> getProperties(){
        return mProperties;
    }

    public Document getDocument(){
        return mDocument;
    }

    public String getID(){
        return (String) getProperty(KEY_ID, null);
    }

    public String getRev(){
        return (String) getProperty(KEY_REV, null);
    }

    public String[] getChannels(){
        return (String[]) getProperty(KEY_CHANNELS, null);
    }

    /**
     * Validates data
     *
     * @param data to be validated
     * @throws DocPropertyValidationException
     */
    private void runValidation(Map<String, Object> data) throws DocPropertyValidationException {
        // get validation rules
        Map<String, Validation_Rules> validation_rules = validationRules();

        if (validation_rules != null) {
            // run validation
            for (Iterator<String> it = validation_rules.keySet().iterator(); it.hasNext(); ) {
                String key = it.next();

                // Required
                if(validation_rules.get(key) == Validation_Rules.REQUIRED){
                    // validate data
                    if(!data.containsKey(key)){
                        throw new DocPropertyValidationException(String.format("%s key is required.", key));
                    }
                }
            }
        }
    }

    /**
     * You can override this method and return custom validation rules.
     * If you override this method, be sure to call super.validationRules().
     *
     * @return By default, it requires the property "_type".
     */
    protected Map<String, Validation_Rules> validationRules() {
        Map<String, Validation_Rules> validation_rules = new HashMap<>();
        validation_rules.put(KEY_TYPE, Validation_Rules.REQUIRED);
        validation_rules.put(KEY_CHANNELS, Validation_Rules.REQUIRED);

        return validation_rules;
    }
}
