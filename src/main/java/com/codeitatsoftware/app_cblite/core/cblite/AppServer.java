package com.codeitatsoftware.app_cblite.core.cblite;

import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by JbalTero on 9/15/2015.
 */
public class AppServer {

    public static String SERVER_HOST ;
    public static String WEB_APP_SERVER_HOST;
    public static String defaultInterface;

    public static void init(String server_host, String web_app_server_host, String port){
        SERVER_HOST =  server_host;
        WEB_APP_SERVER_HOST = web_app_server_host;
        defaultInterface = port;
    }

    /**
     * Called when requesting for complete Sync Url.
     * Only default port interface
     *
     * @return URL: valid URL for push and pull replicators
     */
    public static URL getSyncUrl() {

        URL url = getHostUrl(defaultInterface);

        return url;
    }

    /**
     * Called when requesting for REST API requests.
     *
     * @param INTERFACE adminInterface or defaultInterface
     * @return URL = SERVER_HOST + INTERFACE + DATABASE_NAME
     */
    public static URL getHostUrl(String INTERFACE){
        URL url = null;

        try {
            url = new URL(SERVER_HOST + INTERFACE + CouchbaseLite.DATABASE_NAME);
        } catch (MalformedURLException e) {
            AppLog.e(e);
        }

        return url;
    }

    /**
     * Called when requesting for REST API requests.
     * This does not include the port and databse name.
     * @return URL
     */
    public static URL getWebAppServerUrl(String uri){
        URL url = null;

        try {
            url = new URL(WEB_APP_SERVER_HOST + uri);
        } catch (MalformedURLException e) {
            AppLog.e(e);
        }

        return url;
    }

    public interface Listener{
        void onRequestStart();
        void onSuccess(int statusCode, Header[] headers, byte[] responseBody);
        void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error);
    }

    public static void login(final Listener listener, final String username, String password, String endPoint) throws NullPointerException {

        // Get url
        final String url = AppServer.getWebAppServerUrl(endPoint).toString();

        // Username validation
        if(username.length() == 0){
            throw new NullPointerException("Username should not be null.");
        }

        // Password validation
        if(password.length() == 0){
            throw new NullPointerException("Password should not be null.");
        }

        AsyncHttpClient httpClient = new AsyncHttpClient();

        // Initiate POST Data
        RequestParams params = new RequestParams();
        params.put("username", username );
        params.put("password", password);

        httpClient.post(
                url,
                params,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        AppLog.i("Requesting for session cookie... URL: " + url);

                        listener.onRequestStart();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        // convert byte array to String
                        String JSONString = new String(responseBody);

                        // try to convert JSON String to JSON Object
                        try {
                            JSONObject response = new JSONObject(JSONString);

                            // save session cookie in UserSessionManager for use in replication and API requests.
                            UserSessionManager.getInstance().createNewSession(response, username);

                            listener.onSuccess(statusCode, headers, responseBody);

                        } catch (Throwable t) {
                            AppLog.e(t);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        listener.onFailure(statusCode, headers, responseBody, error);
                    }
                });
    }
}