package com.codeitatsoftware.app_cblite.core.cblite;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.widget.AbsListView;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.ListDialogFragment;
import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.Application;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.codeitatsoftware.app_cblite.helpers.Common;
import com.codeitatsoftware.app_cblite.helpers.Datetime;
import com.codeitatsoftware.app_cblite.helpers.DbActiveRecord;
import com.codeitatsoftware.app_cblite.helpers.PrefsManager;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.Database;
import com.couchbase.lite.Manager;
import com.couchbase.lite.android.AndroidContext;
import com.couchbase.lite.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Database initializations
 */
public class CouchbaseLite{

    private Application mApp = Application.getInstance();

    // Singleton instance
    private static CouchbaseLite sInstance;

    public static final String DATABASE_NAME = Application.APP_NAME.replaceAll("\\s","_").toLowerCase();

    // Global fields
    private Database mDatabase;
    private Synchronize synchronize;

    public static int REQUEST_RESTORE = 11;
    private List<File> backupFileList;

    /**
     * Initialize singleton instance
     *
     */
    public static synchronized void init() {
        if (sInstance == null) {
            sInstance = new CouchbaseLite();
        }
    }

    /**
     * @return singleton instance
     */
    public static CouchbaseLite getInstance() {
        if (sInstance == null) {
            throw new IllegalStateException(CouchbaseLite.class.getSimpleName() +
                    " is not initialized, call init(..) method first.");
        }
        return sInstance;
    }

    public CouchbaseLite(){
        AppLog.v("Instantiated");
    }

    // ============== DATABASE ==============

    /**
     * * Initialize database
     *      create the database manager
     *      get or create the database
     *
     * @throws IOException
     * @throws CouchbaseLiteException
     */
    public void initDatabase() throws IOException, CouchbaseLiteException {
        Manager.enableLogging(AppLog.TAG, Log.VERBOSE);
        Manager.enableLogging(Log.TAG, Log.VERBOSE);
        Manager.enableLogging(Log.TAG_SYNC_ASYNC_TASK, Log.VERBOSE);
        Manager.enableLogging(Log.TAG_SYNC, Log.VERBOSE);
        Manager.enableLogging(Log.TAG_QUERY, Log.VERBOSE);
        Manager.enableLogging(Log.TAG_VIEW, Log.VERBOSE);
        Manager.enableLogging(Log.TAG_DATABASE, Log.VERBOSE);

        // create the database manager with default options
        Manager mManager = new Manager(new AndroidContext(mApp.getApplicationContext()), Manager.DEFAULT_OPTIONS);

        // get or create the database with the provided name
        mDatabase = mManager.getDatabase(DATABASE_NAME);

        // initialize DbActiveRecord with instance of Couchbaselite
        DbActiveRecord.init();

        // Database initiated with <DocumentCount> existing documents.
        AppLog.i("Database '" + DATABASE_NAME + "' initiated with " + mDatabase.getDocumentCount() + " documents. Size: " + Common.bytesToUnitFormat(mDatabase.totalDataSize()));
    }

    /**
     * Gets the current database
     *
     * @return Database
     */
    public Database getDatabase() {
        return mDatabase;
    }

    /**
     * Deletes the current database
     */
    public void deleteDatabase() throws CouchbaseLiteException {
        mDatabase.delete();
        AppLog.d("Database '" + DATABASE_NAME + "' deleted.");
    }

    public synchronized void restore(int position){
        try {
            File backupDB = backupFileList.get(position);

            if (backupDB.exists()) {

                File currentDB = new File(mApp.getApplicationContext().getFilesDir(), DATABASE_NAME + ".cblite");

                AppLog.i(String.format("Backup database. %s -> %s", backupDB.toString(), currentDB.toString()));

                FileChannel src = new FileInputStream(backupDB).getChannel();
                FileChannel dst = new FileOutputStream(currentDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(mApp, "Restore Successful!",
                        Toast.LENGTH_SHORT).show();

                AppLog.i("Restore Successful!");
            }
        } catch (Exception e) {
            Toast.makeText(mApp, "Restore Failed!", Toast.LENGTH_SHORT)
                    .show();

        }
    }

    /**
     * Restore db
     */
    public void showRestore(AppCompatActivity activity) {
        if (!PrefsManager.getInstance().getBoolean("initSynchronize", false)) {
            ListDialogFragment
                    .createBuilder(activity, activity.getSupportFragmentManager())
                    .setTitle("Restore from...")
                    .setItems(getBackupFiles())
                    .setRequestCode(REQUEST_RESTORE)
                    .setCancelButtonText("Cancel")
                    .setConfirmButtonText("Restore")
                    .setChoiceMode(AbsListView.CHOICE_MODE_SINGLE)
                    .show();
        }
        else{
            Toast.makeText(mApp, "Cannot restore when synchronize is enabled.", Toast.LENGTH_SHORT).show();
        }
    }

    private String[] getBackupFiles() {
        File backup = new File(mApp.getExternalFilesDir(null) + "/backup") ;
        File[] backupListFiles = backup.listFiles();
        String[] backupFileNames = new String[0];

        if (backupListFiles != null) {
            //first create a list from String array
            backupFileList = new ArrayList<>(Arrays.asList(backupListFiles));

            //next, reverse the list using Collections.reverse method
            Collections.reverse(backupFileList);

            backupFileNames = new String[backupFileList.size()];
            for (int i = 0; i < backupFileList.size(); i++)
            {
                String timestamp = backupFileList.get(i)
                        .getName()
                        .replace(CouchbaseLite.DATABASE_NAME + "_", "")
                        .replace(".cblite", "");

                backupFileNames[i] = DateUtils.getRelativeTimeSpanString(Long.parseLong(timestamp) * 1000L, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS) + " - " + Datetime.toFormat(timestamp, Datetime.FORMAT_MEDIUM_WEEK);
            }
        }

        return backupFileNames;
    }

    /**
     * Backup db
     */
    public synchronized void backup() {
        if (!PrefsManager.getInstance().getBoolean("initSynchronize", false)) {
            try {
                File sd = new File(mApp.getExternalFilesDir(null) + "/backup") ;

                if (!sd.exists()) {
                   sd.mkdir();
                }

                long last_backup = Datetime.currentTimestamp();

                File currentDB = new File(mApp.getApplicationContext().getFilesDir(), DATABASE_NAME + ".cblite");
                File backupDB = new File(sd, DATABASE_NAME + "_" + last_backup +".cblite");

                AppLog.i(String.format("Backup database. %s -> %s", currentDB.toString(), backupDB.toString()));

                FileChannel src = new FileInputStream(currentDB).getChannel();
                FileChannel dst = new FileOutputStream(backupDB).getChannel();
                dst.transferFrom(src, 0, src.size());
                src.close();
                dst.close();
                Toast.makeText(mApp, "Backup Successful!",
                        Toast.LENGTH_SHORT).show();

                PrefsManager.getInstance().edit().putLong("last_backup", last_backup).apply();

                AppLog.i("Backup Successful!");
            } catch (Exception e) {

                Toast.makeText(mApp, "Backup Failed!", Toast.LENGTH_SHORT)
                        .show();

            }
        }
        else{
            Toast.makeText(mApp, "Cannot restore when synchronize is enabled.", Toast.LENGTH_SHORT).show();
        }
    }

    // ============== REPLICATORS ==============

    /**
     * Called after successful user login in and after database was initiated. This is a continous syncing.
     *
     * @param activity
     */
    public void initSynchronize(Activity activity) throws IOException, CouchbaseLiteException, ParseException {
        // Make sure database is not null
        if(mDatabase == null){
            initDatabase();
        }

        // Convert session expires to date format
        Date expirationDate = Datetime.string_to_date_format(UserSessionManager.getInstance().getExpires(), Datetime.FORMAT_UTC_TIMEZONE);

        synchronize = new Synchronize.Builder(activity, mDatabase, AppServer.getSyncUrl().toString(), true)
                .cookieAuth(UserSessionManager.getInstance().getSessionId(), expirationDate)
                .build();

        PrefsManager.getInstance().edit().putBoolean("initSynchronize", true).apply();
    }

    public Synchronize getSynchronize() {
        return synchronize;
    }
}
