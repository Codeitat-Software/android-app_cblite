package com.codeitatsoftware.app_cblite.core.cblite;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.Application;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.codeitatsoftware.app_cblite.helpers.Datetime;
import com.couchbase.lite.AsyncTask;
import com.couchbase.lite.Database;
import com.couchbase.lite.RevisionList;
import com.couchbase.lite.auth.Authenticator;
import com.couchbase.lite.auth.AuthenticatorFactory;
import com.couchbase.lite.replicator.Replication;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * https://github.com/couchbaselabs/ToDoLite-Android/blob/release/1.1.0/ToDoLite/src/main/java/com/couchbase/todolite/Synchronize.java#L4
 */
public class Synchronize implements Replication.ChangeListener{
    /**
     * Seconds to consider as timeout.
     */
    private int schedule_timeout = 60; // sec

    /**
     * Will automatically stop syncing after this retry
     */
    private int schedule_retry_max = 3;

    // Global fields
    private Application mApp = Application.getInstance();
    private UserSessionManager mUserSessionManager = UserSessionManager.getInstance();
    private CouchbaseLite mCouchbaseLite = CouchbaseLite.getInstance();
    private Context context;
    private Database database;
    public Replication mPullReplication;
    public Replication mPushReplication;
    private boolean facebookAuth;
    private boolean basicAuth;
    private boolean cookieAuth;
    private boolean isRenewingSession = false;
    private Throwable lastError;
    private boolean showToast;
    private ChangeListener changeListener;
    private boolean syncing = false;
    private double progress = -1;
    private int schedule_retried = 0;
    private static final String TAG = "Synchronize";

    public interface ChangeListener{
        void onSync_Start(boolean afterLogin);
        void onSync_Progress(boolean afterLogin, Double progress, Throwable error);
        void onSync_Stop(boolean afterLogin, Throwable error);
        void onSync_Unauthorized();
    }

    private Synchronize(Builder builder) {
        context = builder.context;
        database = builder.database;

        mPullReplication = builder.pullReplication;
        mPushReplication = builder.pushReplication;

        facebookAuth = builder.facebookAuth;
        basicAuth = builder.basicAuth;
        cookieAuth = builder.cookieAuth;

        mPullReplication.addChangeListener(this);
        mPushReplication.addChangeListener(this);
    }

    public void setChangeListener(Synchronize.ChangeListener changeListener){
        this.changeListener = changeListener;
    }

    /**
     * Starts Pull and Push replications
     */
    public void start() {
        mPullReplication.start();
        mPushReplication.start();

        AppLog.i("Sync started.");
    }

    /**
     * Restarts Pull and Push replications
     */
    public void restart() {
        // Call start just to get the progress starting
        onSync_Start(false, 0, null);

        // run asynchronously to prevent blocking UI
        database.runAsync(new AsyncTask() {
            @Override
            public void run(Database database) {
                try {
                    mPullReplication.restart();
                    mPushReplication.restart();

                    AppLog.i("Sync restarted.");
                } catch (Exception e) {
                    Toast.makeText(context, "Problem refreshing.", Toast.LENGTH_SHORT).show();
                    // Replicator is unable to stop thrown after 60 secs.
                    Log.e(TAG, "Replicator is unable to stop.", e);
                }
            }
        });
    }

    /**
     * Called during session renew and restart Pull and Push replications
     * @param response
     */
    public void reinit(JSONObject response) throws ParseException, JSONException {
        // delete previous cookies
        mPullReplication.deleteCookie(mUserSessionManager.getCookieName());
        mPushReplication.deleteCookie(mUserSessionManager.getCookieName());

        // save new session cookie in UserSessionManager
        mUserSessionManager.putSessionCookie(response);

        // Convert session expires to date format
        Date expirationDate = Datetime.string_to_date_format(mUserSessionManager.getExpires(), Datetime.FORMAT_UTC_TIMEZONE);

        mPullReplication.setCookie(mUserSessionManager.getCookieName(), mUserSessionManager.getSessionId(), "/", expirationDate, false, false);
        mPushReplication.setCookie(mUserSessionManager.getCookieName(), mUserSessionManager.getSessionId(), "/", expirationDate, false, false);

        start();

        AppLog.d("Synchronize reinitialized.");
    }

    public void setSyncGatewayChannels(List<String> channels) {
        if (!channels.isEmpty()) {
            mPullReplication.setChannels(channels);
        }
    }

    /**
     * Called during logout.
     */
    public void destroyReplications() {
        mPullReplication.stop();
        mPushReplication.stop();
        mPullReplication.deleteCookie("SyncGatewaySession");
        mPushReplication.deleteCookie("SyncGatewaySession");
        mPullReplication = null;
        mPushReplication = null;

        AppLog.d("Synchronize replicators destroyed.");
    }

    @Override
    public void changed(final Replication.ChangeEvent event) {
        final boolean afterLogin = mUserSessionManager.getAfterLogin();
        Replication replicator = event.getSource();
        Throwable lastError = event.getError();

        // if there's no error
        if(lastError == null){
            // The replication reporting the notification is either
            // the push or the pull, but we want to look at the
            // aggregate of both the push and pull.
            // First check whether replication is currently active:
            boolean active = (mPullReplication.getStatus() == Replication.ReplicationStatus.REPLICATION_ACTIVE) ||
                    (mPushReplication.getStatus() == Replication.ReplicationStatus.REPLICATION_ACTIVE);

            // get last sequence number to shared preferences to be saved later on IDLE
            long lastSequenceNumber = event.getSource().getLocalDatabase().getLastSequenceNumber();

            // get the revision list with the last sequence number stored on session manager.
            RevisionList revisionList = database.changesSince(mUserSessionManager.getLastSequenceNumber(), null, null, null);

            String trigger = (event.getTransition() != null)? "via " + String.valueOf(event.getTransition().getTrigger()): "";

            Log.v(TAG, String.format("%s: %s %s with %d revs", (active? "ACTIVE": "INACTIVE"), (replicator.isPull()? "PULL": "PUSH"), trigger, revisionList.size()));

            // if not active or there's no revision,
            if (!active || revisionList.size() == 0){
                // call stop
                onSync_Stop(afterLogin, null);

                // only change if last sequence number was changed
                if (mUserSessionManager.getLastSequenceNumber() != lastSequenceNumber) {
                    mUserSessionManager.putLastSequenceNumber(lastSequenceNumber);
                    mUserSessionManager.putLastSyncDateTime(String.valueOf(Datetime.currentTimestamp()));
                    Log.v(TAG, "LastSequenceNumber = " + lastSequenceNumber);

                    // hint to call stop
                    progress = -1;
                }
            } else {
                // calculate progress
                double changesCount = mPushReplication.getChangesCount() + mPullReplication.getChangesCount();
                double completed = mPushReplication.getCompletedChangesCount() + mPullReplication.getCompletedChangesCount();

                // avoid exception from dividing to 0
                if (changesCount != 0) {
                    progress = (completed / changesCount) * 100;
                }
                else {
                    // 0 means progress is starting.
                    progress = 0;
                }

                // call start
                onSync_Start(afterLogin, progress, event.getError());

                // call progress
                onSync_Progress(afterLogin, progress, null);
            }
        }
        else{
            Log.i(TAG, "An error occurred during replication.");

            // check if unauthorized then call for session renewal
            if (lastError instanceof HttpResponseException) {
                HttpResponseException responseException = (HttpResponseException) lastError;
                if (responseException.getStatusCode() == 401) {
                    if (!isRenewingSession) {
                        onSync_Unauthorized();
                        isRenewingSession = true;
                    }
                }
            }
            else{
                checkError(replicator);

                // force stop
                onSync_Stop(afterLogin, lastError);
                progress = -1;
            }
        }
    }

    private void onSync_Unauthorized() {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (changeListener != null) {
                    Log.i(TAG, "Replication is Unauthorized. Calling onSync_Unauthorized();");
                    changeListener.onSync_Unauthorized();
                }

                AsyncHttpClient httpClient = new AsyncHttpClient();

                // Get url
                final String url = AppServer.getWebAppServerUrl("/renew_session?db=" + CouchbaseLite.DATABASE_NAME).toString(); // IMPORTANT! No trailing slash

                // Initiate POST Data
                RequestParams params = new RequestParams();
                params.put("username", mUserSessionManager.getUsername());
                params.put("expired_session_id", mUserSessionManager.getSessionId());

                httpClient.post(
                        url,
                        params,
                        new AsyncHttpResponseHandler() {

                            @Override
                            public void onStart() {
                                AppLog.d("Requesting for session renewal...");

                                Toast.makeText(context, "Requesting for session renewal...", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                                // convert byte array to String
                                String JSONString = new String(responseBody);

                                // try to convert JSON String to JSON Object
                                try {
                                    JSONObject response = new JSONObject(JSONString);

                                    mCouchbaseLite.getSynchronize().reinit(response);

                                    Toast.makeText(context, "Re-syncing...", Toast.LENGTH_SHORT).show();
                                } catch (Throwable t) {
                                    AppLog.e(t);
                                }
                            }

                            @Override
                            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                                if (statusCode == 0) {
                                    if ((error instanceof ConnectTimeoutException) || (error instanceof SocketTimeoutException))
                                        Toast.makeText(context, "Bad internet connection. Connection timed out. Try Again.", Toast.LENGTH_SHORT).show();
                                    else
                                        Toast.makeText(context, "Please check your internet connection. Try Again.", Toast.LENGTH_SHORT).show();
                                } else if (statusCode == 401) {
                                    Toast.makeText(context, "Invalid login.", Toast.LENGTH_SHORT).show();
                                } else if (statusCode == 500 && error instanceof HttpResponseException) {
                                    // Internal Server Error
                                    Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                                } else {
                                    // only log the error if none of the above.
                                    AppLog.e(error);
                                }
                            }

                            @Override
                            public void onProgress(long bytesWritten, long totalSize) {
                                String written = String.valueOf(bytesWritten);
                                String total = String.valueOf(totalSize);
                                String percent = String.valueOf((bytesWritten / totalSize) * 100);
                                AppLog.i(percent + "% = " + written + " Bytes / " + total + " Bytes");
                            }

                            @Override
                            public void onRetry(int retryNo) {
                                AppLog.w(retryNo + ": Reconnecting to login... URL: " + url);
                                Toast.makeText(context, "Reconnecting...", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        });
    }

    private void onSync_Start(final boolean afterLogin, double progress, Throwable error) {
        // only if there's a changeListener
        if(changeListener != null){
            // start sync if not syncing and progress >= 0 and no error
            if (!syncing && progress >= 0 && error == null) {
                Log.v(TAG, String.format("Calling onSync_Start(afterLogin = %s, progress = %d, Throwable = %s);", afterLogin, Double.valueOf(progress).intValue(), (error != null) ? error.toString() : "null"));

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // start it
                        changeListener.onSync_Start(afterLogin);
                    }
                });

                syncing = true;

                // schedule "Syncing is taking longer than usual."
                scheduleSyncingAlert();
            }
        }
    }

    private void onSync_Progress(final boolean afterLogin, final double progress, final Throwable error) {
        // only if there's a changeListener
        if(changeListener != null){
            // only if sync started
            if (syncing) {
                Log.v(TAG, String.format("Calling onSync_Progress(afterLogin = %s, progress = %d, Throwable = %s);", afterLogin, Double.valueOf(progress).intValue(), (error != null) ? error.toString() : "null"));

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        // update
                        changeListener.onSync_Progress(afterLogin, progress, error);
                    }
                });
            }
        }
    }

    private void onSync_Stop(final boolean afterLogin, final Throwable error) {
        // only if there's a changeListener
        if (changeListener != null) {
            // stop only if syncing
            if (syncing) {
                Log.v(TAG, String.format("Calling onSync_Stop(afterLogin = %s, Throwable = %s);", afterLogin, (error != null) ? error.toString() : "null"));

                ((Activity)context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        changeListener.onSync_Stop(afterLogin, error);
                    }
                });

                syncing = false;

                if (afterLogin && progress == 100){
                    mUserSessionManager.putAfterLogin(false);
                }
            }
        }
    }

    private void scheduleSyncingAlert() {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        // check if retried reached the schedule retry max
                        if (schedule_retried < schedule_retry_max) {
                            // if still syncing
                            if (syncing && !mUserSessionManager.getAfterLogin()) {
                                showMessage("Syncing is taking longer than usual.");
                                Log.v(TAG, "Syncing is taking longer than usual. syncing: " + syncing);

                                schedule_retried++;

                                // resched
                                scheduleSyncingAlert();
                            }
                        }
                        else{
                            Log.v(TAG, "Reached max retry. Calling force stop.");

                            // force stop
                            onSync_Stop(false, lastError);
                            progress = -1;
                        }
                    }
                }, schedule_timeout * 1000);

                Log.v(TAG, String.format("SCHEDULED in %d secs: Syncing is taking longer than usual.", schedule_timeout));
            }
        });
    }

    private void checkError(Replication replication) {
        // check if current replication error is the same as error before.
        if(lastError != replication.getLastError()){
            // set to current error
            lastError = replication.getLastError();

            Log.v(TAG, String.format("Replication error: %s",  (lastError !=  null) ? lastError.toString(): "null"));

            // Connection refused.
            if (lastError.getCause() instanceof ConnectException){
                showMessage("Syncing failed. Connection refused");
            }
            // Connection timed out.
            else if(lastError instanceof ConnectTimeoutException){
                showMessage("Syncing failed. Connection timed out. Check your internet connection.");
            }
            else{
                AppLog.e(lastError);

                if(showToast)
                    showMessage("Syncing failed.");
            }
        }
    }

    public void showMessage(final String msg) {
        ((Activity)context).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
                showToast = false;
            }
        });
    }

    public static class Builder {
        private Context context;
        private Database database;

        public Replication pullReplication;
        public Replication pushReplication;

        private boolean facebookAuth;
        private boolean basicAuth;
        private boolean cookieAuth;

        public Builder(Context context, Database database, String url, Boolean continuousPull) {
            this.context = context;
            this.database = database;

            if (pullReplication == null && pushReplication == null) {

                URL syncUrl;
                try {
                    syncUrl = new URL(url);
                } catch (MalformedURLException e) {
                    throw new RuntimeException(e);
                }

                pullReplication = database.createPullReplication(syncUrl);
                if (continuousPull)
                    pullReplication.setContinuous(true);

                pushReplication = database.createPushReplication(syncUrl);
                pushReplication.setContinuous(true);
            }
        }

        public Builder facebookAuth(String token) {

            Authenticator facebookAuthenticator = AuthenticatorFactory.createFacebookAuthenticator(token);

            pullReplication.setAuthenticator(facebookAuthenticator);
            pushReplication.setAuthenticator(facebookAuthenticator);

            return this;
        }

        public Builder basicAuth(String username, String password) {

            Authenticator basicAuthenticator = AuthenticatorFactory.createBasicAuthenticator(username, password);

            pullReplication.setAuthenticator(basicAuthenticator);
            pushReplication.setAuthenticator(basicAuthenticator);

            return this;
        }

        public Builder cookieAuth(String cookieValue, Date expirationDate) {
            pullReplication.setCookie("SyncGatewaySession", cookieValue, "/", expirationDate, false, false);
            pushReplication.setCookie("SyncGatewaySession", cookieValue, "/", expirationDate, false, false);

            return this;
        }

        public Builder addChangeListener(Replication.ChangeListener changeListener) {
            pullReplication.addChangeListener(changeListener);
            pushReplication.addChangeListener(changeListener);

            return this;
        }

        public Synchronize build() {
            return new Synchronize(this);
        }

    }

}
