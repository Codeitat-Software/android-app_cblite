package com.codeitatsoftware.app_cblite.core.exceptions;

/**
 * Created by JbalTero on 8/20/2015.
 */
public class DocPropertyValidationException extends Throwable {
    public DocPropertyValidationException() {
    }

    public DocPropertyValidationException(String detailMessage) {
        super(detailMessage);
    }
}
