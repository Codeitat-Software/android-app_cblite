package com.codeitatsoftware.app_cblite.core;

import android.content.Context;
import android.util.Log;

import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.codeitatsoftware.app_cblite.helpers.Datetime;
import com.codeitatsoftware.app_cblite.helpers.DbActiveRecord;
import com.couchbase.lite.CouchbaseLiteException;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

/**
 * Created by JbalTero on 5/9/2015.
 */
public class AppLog {

    public static String TAG = CouchbaseLite.DATABASE_NAME;
    public static final String FILENAME_LOGCAT = "log.txt";
    private static Application app = Application.getInstance();

    /**
     * Do you want to see debug logs?
     */
    public static Boolean SHOW_DEBUG = true;

    /**
     * Do you want to see logs?
     */
    public static Boolean SHOW_LOG = true;

    /**
     * Do you want to write the logs in a file?
     */
    public static Boolean WRITE_LOG = true;

    /**
     * Override default TAG
     * @param TAG
     */
    public static void setTAG(String TAG) {
        AppLog.TAG = TAG;
    }

    // ========== LOGGING ==========

    /**
     * Verbose
     *
     * @param msg
     */
    public static void v(final String msg) {
        String log = format(classInfo(), msg);

        if (SHOW_LOG)
            Log.v(TAG, log);

        writeLog("v| " + log);
    }

    /**
     * Debug
     *
     * @param msg
     */
    public static void d(final String msg) {
        String log = format(classInfo(), msg);

        if (SHOW_LOG && SHOW_DEBUG)
            Log.d(TAG, log);

        writeLog("d| " + log);
    }

    /**
     * Information
     *
     * @param msg
     */
    public static void i(final String msg) {
        String log = format(classInfo(), msg);

        if (SHOW_LOG)
            Log.i(TAG, log);

        writeLog("i| " + log);
    }

    /**
     * Warning
     *
     * @param msg
     */
    public static void w(final String msg) {
        String log = format(classInfo(), msg);

        if (SHOW_LOG)
            Log.w(TAG, log);

        writeLog("i| " + log);

        if (Application.ENVIRONMENT == Application.env.PRODUCTION) {
            // Create document for warning report
            HashMap<String, Object> data = new HashMap<>();
            data.put("type", "report");
            data.put("level", "warning");
            data.put("message", log);
            // TODO: 8/20/2015 Use Datetime class
            data.put("created_on", String.valueOf(Datetime.currentTimestamp()));
            data.put("created_by", app.getUserSessionManager().getUsername());
            try {
                DbActiveRecord.create(data, String.valueOf(Datetime.currentTimestamp()) + "-report");
            } catch (CouchbaseLiteException e) {
                AppLog.e(e);
            }
        }
    }

    /**
     * Error
     *
     * @param throwable
     */
    public static void e(final Throwable throwable) {
        // print stack trace
        throwable.printStackTrace();

        // get the stack trace string
        String log = "\n\n========== START OF ERROR ==========\n";
        log += format(classInfo(), Log.getStackTraceString(throwable));
        log += "\n========== END OF ERROR ==========\n";

        if (SHOW_LOG)
            Log.e(TAG, log);

        writeLog("e| " + log);

        if (Application.ENVIRONMENT == Application.env.PRODUCTION) {
            // // FIXME: 8/12/2015
//        String unique = throwable.getMessage() + ":cause" + throwable.getCause().getMessage();

            // Create document for error report
            HashMap<String, Object> data = new HashMap<String, Object>();
            data.put("type", "report");
            data.put("level", "error");
//        data.put("id", String.valueOf(unique.getBytes()) + ":" + String.valueOf(unique.hashCode()));
            data.put("message", log);
            // TODO: 8/12/2015  Use Datetime
            data.put("created_on", String.valueOf(Datetime.currentTimestamp()));
            data.put("created_by", app.getUserSessionManager().getUsername());

            try {
                DbActiveRecord.create(data, String.valueOf(Datetime.currentTimestamp()) + "-report");
            } catch (CouchbaseLiteException e) {
                AppLog.e(e);
            }
        }
    }

    /**
     * Get fullClassName, className, methodName and lineNumber
     *
     * @return String
     */
    private static String classInfo(){
        final String fullClassName = Thread.currentThread().getStackTrace()[4].getClassName();
        final String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
        final String methodName = Thread.currentThread().getStackTrace()[4].getMethodName();
        final int lineNumber = Thread.currentThread().getStackTrace()[4].getLineNumber();

        return className + "." + methodName + "() line:" + lineNumber + " | ";
    }

    /**
     * Format string for log output
     *
     * @param classInfo
     * @param msg
     * @return
     */
    private static String format(String classInfo, String msg) {
        return classInfo + msg;
    }

    // ========== LOGGING FILE READ/WRITE ==========

    /**
     * Read specified log file
     *
     * @return String
     * @param context
     */
    public static String readLog(Context context){
        BufferedReader br;
        String response = null;
        try {
            StringBuffer output = new StringBuffer();
            File logFile = new File(context.getApplicationContext().getExternalFilesDir(null), FILENAME_LOGCAT);

            if(logFile.exists()){
                br = new BufferedReader(new FileReader(logFile));
                String line;
                while ((line = br.readLine()) != null) {
                    output.append(line +"\n");
                }
                response = output.toString();
            }

        } catch (IOException e) {
            AppLog.e(e);
            return null;
        }
        return response;
    }

    /**
     * Write log to file
     *
     * @param log message
     */
    private static void writeLog(String log){
        if (WRITE_LOG) {

            // TODO: 8/16/2015 Compare performance trace
            log = Datetime.toFormat(Datetime.currentTimestamp(), "yyyy-MM-dd hh:mm a") + " : "+ log;

            File logFile = new File(app.getExternalFilesDir(null), FILENAME_LOGCAT);

            // create log file if it doesn't exists
            if (!logFile.exists()) {
                try {
                    logFile.createNewFile();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }

            if (Application.ENVIRONMENT == Application.env.PRODUCTION) {
                // try to write the log.
                try {

                    //BufferedWriter for performance, true to set append to file flag
                    BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                    buf.append(log);
                    buf.newLine();

                    buf.close();
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            }
        }
    }

    /**
     * Called during user logout.
     *
     * @param context
     * @return True if deleted, else false
     */
    public static boolean deleteLog(Context context){
        File log = new File(context.getExternalFilesDir(null), AppLog.FILENAME_LOGCAT);
        return log.delete();
    }



}
