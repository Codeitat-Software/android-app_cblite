package com.codeitatsoftware.app_cblite.ui;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import com.codeitatsoftware.app_cblite.R;
import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.Application;

import java.io.File;

public class CrashReport extends Activity implements View.OnClickListener{

    private String mMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE); // make a dialog without a titlebar
        setFinishOnTouchOutside(false); // prevent users from dismissing the dialog by tapping outside
        setContentView(R.layout.activity_crash_report);

        Bundle extras = getIntent().getExtras();
        mMessage = extras.getString("message");

        Button btnSend = (Button)findViewById(R.id.btnSend);
        btnSend.setOnClickListener(this);

        Button btnBack = (Button)findViewById(R.id.btnBack);
        btnBack.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        int i = v.getId();
        if (i == R.id.btnSend) {
            sendLogFile();
        }
        else if (i == R.id.btnBack) {
            restartApp();
        }
    }

    private void restartApp() {
        // TODO: 8/20/2015 Restart app when clicking back during crash report
        onBackPressed();
    }

    private void sendLogFile() {

        File logFile = new File(getExternalFilesDir(null), AppLog.FILENAME_LOGCAT);

        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("plain/text");
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{Application.DEVELOPER_EMAIL});
        intent.putExtra(Intent.EXTRA_SUBJECT, String.format("%s CRASH REPORT", Application.APP_NAME));
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + logFile.getPath()));
        intent.putExtra(Intent.EXTRA_TEXT, mMessage + "\n\n\nLog file attached."); // do this so some email clients don't complain about empty body.
        startActivity(intent);
    }

}
