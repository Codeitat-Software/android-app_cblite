package com.codeitatsoftware.app_cblite.ui;

import android.content.Intent;
import android.os.Handler;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Toast;

import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.codeitatsoftware.app_cblite.core.cblite.AppServer;
import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.couchbase.lite.CouchbaseLiteException;
import com.couchbase.lite.RevisionList;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.steamcrafted.loadtoast.LoadToast;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;

import java.net.SocketTimeoutException;

/**
 * Created by JbalTero on 8/4/2015.
 */
public class LogoutDialog extends SimpleDialogFragment {

    private CouchbaseLite mCouchbaseLite = CouchbaseLite.getInstance();
    private UserSessionManager mUserSessionManager = UserSessionManager.getInstance();

    private FragmentActivity activity;
    private LoadToast mLoadToast;
    private Intent intent;
    private String message;
    private String title;

    /**
     * Show this dialog
     *
     * @param activity
     */
    public static void show(FragmentActivity activity, Intent intent) {
        LogoutDialog dialog = new LogoutDialog();
        dialog.setActivity(activity);
        dialog.setIntent(intent);
        dialog.show(activity.getSupportFragmentManager(), "LogoutDialog");
    }

    private void setActivity(FragmentActivity fragmentActivity) {
        this.activity = fragmentActivity;
    }

    @Override
    public Builder build(Builder builder) {

        initView();

        builder.setTitle(title);
        builder.setPositiveButton("Ok", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logout();
                dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        builder.setMessage(message);

        return builder;
    }

    private void logout() {
        // Get url
        final String url = AppServer.getWebAppServerUrl("/logout?db=" + CouchbaseLite.DATABASE_NAME).toString(); // IMPORTANT! No trailing slash

        AsyncHttpClient httpClient = new AsyncHttpClient();

        // Initiate POST Data
        RequestParams params = new RequestParams();
        params.put("username", mUserSessionManager.getUsername());
        params.put("session_id", mUserSessionManager.getSessionId());

        httpClient.post(
                url,
                params,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        AppLog.i("Requesting logout session for " + mUserSessionManager.getUsername() + "... URL: " + url);
                        mLoadToast.setText("Logging out...");
                        mLoadToast.show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        AppLog.i("Session successfully deleted from the server.");

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    mUserSessionManager.clear(activity, intent);
                                    mLoadToast.success();
                                } catch (CouchbaseLiteException e) {
                                    AppLog.e(e);
                                }
                            }
                        }, 1500);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        mLoadToast.error();

                        if (statusCode == 0) {
                            if ((error instanceof ConnectTimeoutException) || (error instanceof SocketTimeoutException)) {
                                AppLog.d("Bad internet connection. Connection timed out. Try Again.");
                                Toast.makeText(activity, "Bad internet connection. Connection timed out. Try Again.", Toast.LENGTH_SHORT).show();
                            } else {
                                AppLog.d("Please check your internet connection. Try Again.");
                                Toast.makeText(activity, "Please check your internet connection. Try Again.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (statusCode == 404) {
                            Toast.makeText(activity, "Session not found for user", Toast.LENGTH_SHORT).show();

                        } else if (statusCode == 500 && error instanceof HttpResponseException) {
                            // Internal Server Error
                            Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            // only log the error if none of the above.
                            AppLog.e(error);
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        String written = String.valueOf(bytesWritten);
                        String total = String.valueOf(totalSize);
                        String percent = String.valueOf((bytesWritten / totalSize) * 100);
                        AppLog.i(percent + "% = " + written + " Bytes / " + total + " Bytes");
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        AppLog.w(retryNo + ": Reconnecting to logout... URL: " + url);
                        Toast.makeText(activity, "Reconnecting...", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initView() {
        mLoadToast = new LoadToast(activity);

        // get the revision list with the last sequence number stored on session manager.
        RevisionList revisionList = mCouchbaseLite.getDatabase().changesSince(mUserSessionManager.getLastSequenceNumber(), null, null, null);

        // if empty, ready to logout...
        if(revisionList.isEmpty()){
            title = "Logout";
            message = "Do you really want to logout?";
        }
        else{
            title = "Pending Syncing";
            message = "There are changes that needs to be synced and will not be saved if you choose to logout. Are you sure you want to logout?";
        }
    }

    public void setIntent(Intent intent) {
        this.intent = intent;
    }
}
