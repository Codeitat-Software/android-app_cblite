package com.codeitatsoftware.app_cblite.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.codeitatsoftware.app_cblite.R;

public class About extends AppCompatActivity {

    public static void start(Context context, int version_code, String version_name){
        Intent intent = new Intent(context, About.class);
        intent.putExtra("VERSION_NAME", version_name);
        intent.putExtra("VERSION_CODE", version_code);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        String version_name = getIntent().getStringExtra("VERSION_NAME");

        TextView versionName = (TextView) findViewById(R.id.textView_versionName);
        versionName.setText(version_name);
    }
}
