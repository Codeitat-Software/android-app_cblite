package com.codeitatsoftware.app_cblite.ui;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.avast.android.dialogs.core.BaseDialogFragment;
import com.avast.android.dialogs.fragment.SimpleDialogFragment;
import com.codeitatsoftware.app_cblite.R;
import com.codeitatsoftware.app_cblite.core.AppLog;
import com.codeitatsoftware.app_cblite.core.Application;
import com.codeitatsoftware.app_cblite.core.cblite.AppServer;
import com.codeitatsoftware.app_cblite.core.cblite.CouchbaseLite;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.codeitatsoftware.app_cblite.helpers.AppError;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import net.steamcrafted.loadtoast.LoadToast;

import org.apache.http.Header;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;
import org.json.JSONObject;

import java.net.SocketTimeoutException;

/**
 * Created by JbalTero on 8/19/2015.
 */
public class ChangePasswordDialog extends SimpleDialogFragment {
    private Application app = Application.getInstance();
    private CouchbaseLite cblite = app.getCouchbaseLite();
    private UserSessionManager userSessionManager = app.getUserSessionManager();

    private View mView;
    private EditText mEditTextCurrentPassword;
    private EditText mEditTextNewPassword;
    private EditText mEditTextConfirmNewPassword;
    private LoadToast mLoadToast;

    public static void show(FragmentActivity activity) {
        ChangePasswordDialog dialog = new ChangePasswordDialog();

        dialog.show(activity.getSupportFragmentManager(), "ChangePasswordDialog");
    }

    @Override
    public BaseDialogFragment.Builder build(BaseDialogFragment.Builder builder) {

        initView();

        builder.setTitle("Change Password");
        builder.setView(mView);
        builder.setPositiveButton("Submit", new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(!formValidator()){
                    return;
                }

                changePassword();
                dismiss();
            }
        });
        builder.setNegativeButton("Cancel", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return builder;
    }

    private void changePassword() {
        // Get url
        final String url = AppServer.getWebAppServerUrl("/change_password?db=" + CouchbaseLite.DATABASE_NAME).toString(); // IMPORTANT! No trailing slash

        AsyncHttpClient httpClient = new AsyncHttpClient();

        // Initiate POST Data
        RequestParams params = new RequestParams();
        params.put("username", userSessionManager.getUsername());
        params.put("current_password", mEditTextCurrentPassword.getText().toString());
        params.put("new_password", mEditTextNewPassword.getText().toString());

        final Context context = getActivity();

        httpClient.post(
                url,
                params,
                new AsyncHttpResponseHandler() {

                    @Override
                    public void onStart() {
                        AppLog.i("Requesting for change password... URL: " + url);
                        mLoadToast.setText("Submitting...");
                        mLoadToast.show();
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                        AppLog.i("Password successfully changed.");

                        // convert byte array to String
                        String JSONString = new String(responseBody);

                        // try to convert JSON String to JSON Object
                        try {
                            JSONObject response = new JSONObject(JSONString);

                            AppLog.i("Saving new session cookie");

                            cblite.getSynchronize().reinit(response);

                            mLoadToast.success();
                        } catch (Throwable t) {
                            AppLog.e(t);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                        mLoadToast.error();

                        if (statusCode == 0) {
                            if ((error instanceof ConnectTimeoutException) || (error instanceof SocketTimeoutException)){
                                AppLog.d("Bad internet connection. Connection timed out. Try Again.");
                                Toast.makeText(context, "Bad internet connection. Connection timed out. Try Again.", Toast.LENGTH_SHORT).show();
                            }
                            else{
                                AppLog.d("Please check your internet connection. Try Again.");
                                Toast.makeText(context, "Please check your internet connection. Try Again.", Toast.LENGTH_SHORT).show();
                            }
                        } else if (statusCode == 401) {
                            Toast.makeText(context, "Invalid current password", Toast.LENGTH_SHORT).show();

                        } else if (statusCode == 500 && error instanceof HttpResponseException) {
                            // Internal Server Error
                            Toast.makeText(context, error.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            // only log the error if none of the above.
                            AppLog.e(error);
                        }
                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {
                        String written = String.valueOf(bytesWritten);
                        String total = String.valueOf(totalSize);
                        String percent = String.valueOf((bytesWritten / totalSize) * 100);
                        AppLog.i(percent + "% = " + written + " Bytes / " + total + " Bytes");
                    }

                    @Override
                    public void onRetry(int retryNo) {
                        AppLog.w(retryNo + ": Reconnecting to logout... URL: " + url);
                        Toast.makeText(context, "Reconnecting...", Toast.LENGTH_SHORT).show();
                    }
                });
    }

    private void initView() {
        mView = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_change_password_form, null);

        mEditTextCurrentPassword = (EditText) mView.findViewById(R.id.cs_editTextCurrentPassword);
        mEditTextNewPassword = (EditText) mView.findViewById(R.id.cs_editTextNewPassword);
        mEditTextConfirmNewPassword = (EditText) mView.findViewById(R.id.cs_editTextConfirmNewPassword);

        mLoadToast = new LoadToast(getActivity());
    }

    private boolean formValidator(){

        if(mEditTextCurrentPassword.getText().toString().isEmpty()){
            mEditTextCurrentPassword.setError(AppError.mustNotBeEmpty("Current Password"));
            mEditTextCurrentPassword.requestFocus();
            return false;
        }

        if(mEditTextNewPassword.getText().toString().isEmpty()){
            mEditTextNewPassword.setError(AppError.mustNotBeEmpty("New Password"));
            mEditTextNewPassword.requestFocus();
            return false;
        }

        if(mEditTextConfirmNewPassword.getText().toString().isEmpty()){
            mEditTextConfirmNewPassword.setError(AppError.mustNotBeEmpty("Confirm Password"));
            mEditTextConfirmNewPassword.requestFocus();
            return false;
        }

        if(!mEditTextNewPassword.getText().toString().equals(mEditTextConfirmNewPassword.getText().toString())){
            mEditTextConfirmNewPassword.setError(AppError.invalid("Confirm password"));
            mEditTextConfirmNewPassword.requestFocus();
            return false;
        }

        return true;
    }
}
