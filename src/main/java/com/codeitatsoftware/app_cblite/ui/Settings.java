package com.codeitatsoftware.app_cblite.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.format.DateUtils;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.codeitatsoftware.app_cblite.R;
import com.codeitatsoftware.app_cblite.core.Application;
import com.codeitatsoftware.app_cblite.core.UserSessionManager;
import com.codeitatsoftware.app_cblite.helpers.AutoUpdateApk;
import com.codeitatsoftware.app_cblite.helpers.Datetime;
import com.codeitatsoftware.app_cblite.helpers.PrefsManager;

import java.util.Observable;
import java.util.Observer;

public class Settings extends AppCompatActivity implements View.OnClickListener, Observer {

    private Application app = Application.getInstance();
    private UserSessionManager userSessionManager = app.getUserSessionManager();

    private static Intent redirectIntent;
    private CheckBox wifi_only;

    public static void start(Context context, Intent redIntent){
        redirectIntent = redIntent;

        Intent intent = new Intent(context, Settings.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        // Creating The Toolbar and setting it as the Toolbar for the activity
        Toolbar mToolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(mToolbar);

        setTitle(userSessionManager.getUsername());

        Button mChangePassword = (Button) findViewById(R.id.btnChangePassword);
        Button mLogout = (Button) findViewById(R.id.btnLogout);
        Button mCheckForUpdate = (Button) findViewById(R.id.btnCheckForUpdate);
        TextView textViewLastDataSyncValue = (TextView) findViewById(R.id.textViewLastDataSyncValue);

        String syncDateTime = userSessionManager.getLastSyncDateTime();

        // Avoid Invalid Long exception.
        long lastSyncDateTime = 0;
        if (syncDateTime != null) {
            lastSyncDateTime = Long.parseLong(syncDateTime);

            String timeSpan = (String) DateUtils.getRelativeTimeSpanString(lastSyncDateTime*1000L, System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS);
            String datetime = Datetime.toFormat(lastSyncDateTime, Datetime.FORMAT_MEDIUM_WEEK);
            textViewLastDataSyncValue.setText(timeSpan + " - " + datetime);
        }

        mChangePassword.setOnClickListener(this);
        mLogout.setOnClickListener(this);
        mCheckForUpdate.setOnClickListener(this);

        wifi_only = (CheckBox) findViewById(R.id.chboxWiFiOnly);
        wifi_only.setChecked(PrefsManager.getInstance().getBoolean("enableMobileUpdates", false));
        wifi_only.setOnClickListener(this);

        app.getAutoUpdateApk().addObserver(this);

    }

    @Override
    public void onClick(View view) {
        int i = view.getId();
        if (i == R.id.btnChangePassword) {
            ChangePasswordDialog.show(this);

        } else if (i == R.id.btnLogout) {
            LogoutDialog.show(this, redirectIntent);
        } else if (i == R.id.btnCheckForUpdate) {
            app.getAutoUpdateApk().checkUpdatesManually();
        } else if (i == R.id.chboxWiFiOnly) {
            if(wifi_only.isChecked()){
                AutoUpdateApk.enableMobileUpdates();
                PrefsManager
                        .getInstance()
                        .edit()
                        .putBoolean("enableMobileUpdates", true)
                        .apply();
            }
            else{
                AutoUpdateApk.disableMobileUpdates();
                PrefsManager
                        .getInstance()
                        .edit()
                        .putBoolean("enableMobileUpdates", false)
                        .apply();
            }
        }
    }

    @Override
    public void update(Observable observable, final Object data) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                String msg = (String) data;

                if(msg.equals(AutoUpdateApk.AUTOUPDATE_CHECKING)){
                    Toast.makeText(Settings.this, "Checking for update. Please wait...", Toast.LENGTH_SHORT).show();
                }
                else if(msg.equals(AutoUpdateApk.AUTOUPDATE_GOT_UPDATE)){
                    Toast.makeText(Settings.this, "There is an update.", Toast.LENGTH_SHORT).show();
                }
                else if(msg.equals(AutoUpdateApk.AUTOUPDATE_HAVE_UPDATE)){
                    Toast.makeText(Settings.this, "Update to latest version.", Toast.LENGTH_SHORT).show();
                }
                else if(msg.equals(AutoUpdateApk.AUTOUPDATE_NO_UPDATE)){
                    Toast.makeText(Settings.this, "No update available.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
